FROM jbarlow83/ocrmypdf

USER root
RUN mkdir /data
ADD ocrmypdf-entry.sh /application/ocrmypdf-entry.sh
USER docker

ENTRYPOINT ["/application/ocrmypdf-entry.sh"]