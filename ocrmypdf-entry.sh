#!/bin/bash

###########################
### Define paths        ###
###########################
basePath=/data
searchPath=(${SEARCHPATH//;/ })


createAvailableFileList() {
	find $basePath \( ! -regex '.*/\..*' \) -path "*$1*" -type f -name "*.pdf" > /tmp/available.list
	if [ $? -ne 0 ]; then echo "ERROR - cannot create available file list for $1 (find)!"; exit 1; fi
	sort /tmp/available.list -o /tmp/available_sorted.list
	if [ $? -ne 0 ]; then echo "ERROR - cannot create available file list for $1 (sort)!"; exit 1; fi
}

createProcessedFileList() {
	find $basePath \( -regex '.*/\.orig/.*' \) -path "*$1*" -type f -name "*.pdf" > /tmp/processed.list
	if [ $? -ne 0 ]; then echo "ERROR - cannot create processed file list for $1 (find)!"; exit 1; fi
	sed -i.bak "s/\/\.orig\//\//g" /tmp/processed.list
	if [ $? -ne 0 ]; then echo "ERROR - cannot create processed file list for $1 (sed)!"; exit 1; fi
	sort /tmp/processed.list -o /tmp/processed_sorted.list
	if [ $? -ne 0 ]; then echo "ERROR - cannot create processed file list for $1 (sort)!"; exit 1; fi
}

identifyChanges() {
	diff /tmp/processed_sorted.list /tmp/available_sorted.list > /tmp/tasks.list
	if [ $? -gt 1 ]; then echo "ERROR - during task list creation (diff)!" $?; exit 1; fi
}

copyOriginalToBackup() {
	mkdir -p "$(dirname "$1")/.orig"
	touch "$(dirname "$1")/.orig/.noindex"
	cp -n "$1" "$(dirname "$1")/.orig/$(basename "$1")"
}

runOcrOnList() {
	while read in 
	do 
		# echo "DEBUG - task line: " $in
		if [[ $in == \>* ]]
 		then 
 			pdfFile="${in#*\ }";
 			echo "INFO - Processing $pdfFile";
 			copyOriginalToBackup "$pdfFile"
 			# Only proceed if backup copy was successfully created
 			if [ $? -eq 0 ]; then
	 			ocrmypdf -l deu --skip-text "$pdfFile" "$pdfFile.ocr";
	 			if [ $? -eq 0 ]; then
	        		rm "$pdfFile"	
		 			mv "$pdfFile.ocr" "$pdfFile"
	 				echo "INFO - Processing for $pdfFile finished successful"
	 			else
	 				echo "ERROR - Processing for $pdfFile failed"
			    fi
			else
				echo "ERROR - skipping further process for file $pdfFile because of an error." $?
			fi
 		fi 
 	done < /tmp/tasks.list;
}



############
### Main ###
############
if [ ! -z "$searchPath" ] 
	then
		. /appenv/bin/activate
		cd /home/docker
		for i in "${searchPath[@]}"
		do
			echo "INFO - starting ocr process for searchPath $i"
			createAvailableFileList "$i"
			createProcessedFileList "$i"
			identifyChanges
			runOcrOnList 
			echo "INFO - finished ocr process for searchPath $i"
		done
	else
		echo "ERROR - a valid search path must be defined in the SEARCHPATH environment variable";
fi